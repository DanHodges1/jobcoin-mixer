### Intro
This is a Scala App that compiles to JS, and runs on Node.

Why Node, one might ask?
Partially due to expedience - I'm quite familiar with running Scala in JS environments, whether the browser, or node.
I also wanted to demonstrate my ability to create facades around JS types. I've found that skill invaluable in an enterprise context many times.

For expedience, I also used a library called diode- https://github.com/suzaku-io/diode.
I've used diode extensively in browser based scala apps, and I find it appropriate for smaller apps where larger, distributed event sourcing solutions are over the top.

### Structure
Most the magic happens, as one might assume, in Jobcoin Mixer.
I chose to use a map of ID => Mixer to represent mixers in app state.
Although that's not strictly necessary to get to MVP, I find it easier to deal with a map of a single item than having to refactor later, as it would seem pretty reasonable to add multiple mixers.

I choose to transfer funds directly from the mixers deposit into the house, then to the mixers profit address, without additional mixing (meta-mixer-mixing? :)).
I could be convinced to do this differently. 

My implementation divides incoming deposits by the number of addresses and doles them out evenly over the course of a randomized amount of seconds from 1-30.


### Given more time
I would consider revisiting the transfer of profits.
I would test sad paths more completely.
I would consider a more complex / randomized / obscure-to-observer distribution strategy.
I would setup a single sbt command to build and run the app- node cli apps terminate early when run via `sbt run`- I need to explore that more.


### Building / Running / Testing
to build `sbt fullOptJS::webpack`

to run `node target/scala-2.13/scalajs-bundler/main/jobcoin-mixer-opt.js`

to test `sbt test`






