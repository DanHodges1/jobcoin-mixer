package jobcoin

import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

trait Spec extends AnyFlatSpec with Matchers with BeforeAndAfterEach
