package jobcoin

class TransferSpec extends Spec {
  "a transfer" should "properly build query strings with Some(from)" in {
    assert(
      Transfer("midday", "bob", Some("dave"), "55.21").queryString == "?fromAddress=dave&toAddress=bob&amount=55.21"
    )
  }

  "a transfer" should "properly build query strings when from is empty" in {
    assert(
      Transfer(
        "midday",
        toAddress = "Bill",
        fromAddress = None,
        "55.21"
      ).queryString == "?toAddress=Bill&amount=55.21"
    )
  }
}
