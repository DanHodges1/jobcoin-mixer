package jobcoin

import diode.{Action, ActionHandler, ActionResult}
import diode._
import jobcoin.CircuitSpec.TestCircuit
import jobcoin.House.JobcoinNetworkHouse
import jobcoin.Mixer.DefaultMixerID

abstract class CircuitSpec() extends Spec with RootCircuitOps {

  override def appContext: AppContext = new TestAppContext

  class BaseCircuit extends TestCircuit { self =>
    var actions: Seq[Action] = Seq.empty

    val actionLogger = new ActionHandler(zoomTo(_.mixers)) {

      override def handle: PartialFunction[Any, ActionResult[AppState]] = {
        case ab: ActionBatch =>
          ab.actions.foreach(a => self.apply(a.asInstanceOf[Action]))
          noChange
        case a: Action =>
          actions = actions.appended(a)
          noChange
      }
    }

    override def initialModel: AppState = Model

    override def actionHandler: HandlerFunction =
      foldHandlers(
        actionLogger,
        super.actionHandler
      )
  }

  def Model =
    AppState(
      mixers = Map(DefaultMixerID -> Mixer.defaultState),
      house = JobcoinNetworkHouse(appContext.gateway)
    )

  var circuit: BaseCircuit = new BaseCircuit

  override def afterEach(): Unit = {
    circuit = new BaseCircuit
  }

}

object CircuitSpec {
  abstract class TestCircuit extends RootCircuitOps {
    override def appContext: AppContext = new TestAppContext {}
  }
}
