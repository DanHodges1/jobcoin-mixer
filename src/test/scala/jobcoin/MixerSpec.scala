package jobcoin

import jobcoin.House.DoleOut
import jobcoin.Mixer.{FundsReceived, MonitorDeposits, ToggleMixerPolling, Tumble, UpdateMixerDestinations}
import jobcoin.TestAppContext.TestGateway

import scala.math.BigDecimal

class MixerSpec extends CircuitSpec {
  def mixer: Option[Mixer] = circuit.zoom(_.mixers.get(Mixer.DefaultMixerID)).value

  "Mixer" should "toggle polling" in {
    circuit(ToggleMixerPolling(Mixer.DefaultMixerID, true))
    assert(mixer.exists(_.continuouslyPoll))
  }

  "Mixer" should "update destinations" in {
    val destinations = Seq("dest-1", "dest-2", "dest-3")
    circuit(UpdateMixerDestinations(Mixer.DefaultMixerID, destinations))
    assert(mixer.exists(_.destinations == destinations))
  }

  "Mixer" should "monitor deposits and receive funds" in {
    val destinations = Seq("dest-1", "dest-2", "dest-3")
    circuit(UpdateMixerDestinations(Mixer.DefaultMixerID, destinations))
    circuit(MonitorDeposits(Mixer.DefaultMixerID))

    assert(
      circuit.actions.exists {
        case FundsReceived(key, transfer) => true
        case _                            => false
      }
    )
  }

  "Mixer" should "receive and tumble funds" in {
    circuit(
      FundsReceived(
        Mixer.DefaultMixerID,
        Transfer(
          timestamp = TestGateway.tomorrow.toISOString(),
          toAddress = circuit.zoom(_.house).value.address,
          fromAddress = mixer.map(_.address),
          amount = "5.00"
        )
      )
    )

    assert(
      circuit.actions.exists {
        case Tumble("default-mixer", amount) => amount == BigDecimal("5.00")
        case _                               => false
      }
    )
  }

  "Mixer" should "distribute funds properly" in {
    val destinations = Seq("dest-1", "dest-2", "dest-3")
    assert(
      Mixer.distributions(BigDecimal(6.00), destinations) == Seq(
        DoleOut(BigDecimal(1.96), "dest-1"),
        DoleOut(BigDecimal(1.96), "dest-2"),
        DoleOut(BigDecimal(1.96), "dest-3")
      )
    )
  }

  "Mixer" should "calculate profits" in {
    assert(Mixer.profit(BigDecimal(6.00)) == BigDecimal(0.12))
  }
}
