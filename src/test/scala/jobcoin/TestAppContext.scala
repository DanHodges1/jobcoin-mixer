package jobcoin
import jobcoin.Mixer.DefaultMixerAddress
import jobcoin.TestAppContext.{RunNow, TestGateway}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.concurrent.ExecutionContextExecutor
import scala.scalajs.js.Date

class TestAppContext extends AppContext {
  override def variableDelay: FiniteDuration = Duration(0, "seconds")
  override def pollInterval: FiniteDuration = Duration(0, "seconds")

  override def gateway: Gateway = TestGateway
  override def executionContext: ExecutionContext = RunNow
}

object TestAppContext {
  object RunNow extends ExecutionContextExecutor {

    def execute(runnable: Runnable): Unit =
      try runnable.run()
      catch {
        case t: Throwable => reportFailure(t)
      }

    def reportFailure(t: Throwable): Unit =
      t.printStackTrace()
  }

  object TestGateway extends Gateway {
    override def checkBalance(address: Address): Future[Either[TestGateway.ErrorMessage, Amount]] = ???
    override def transferFunds(transfer: Transfer): Future[Either[TestGateway.ErrorMessage, Unit]] =
      Future.successful(Right(()))

    var tomorrow = new Date()
    tomorrow.setDate(tomorrow.getDate() + 1)

    override def fetchAddressInfo(address: Address): Future[Either[TestGateway.ErrorMessage, AddressInfo]] =
      Future.successful(
        Right(
          AddressInfo(
            balance = "20.00",
            transactions = Seq(
              Transfer(tomorrow.toISOString(), DefaultMixerAddress, None, "6.00")
            )
          )
        )
      )
  }
}
