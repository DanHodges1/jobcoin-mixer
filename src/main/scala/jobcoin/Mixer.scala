package jobcoin

import diode.{Action, ActionBatch, ActionHandler, ActionResult, Effect, ModelRW, NoAction}
import jobcoin.House.DoleOut
import diode.Implicits.runAfterImpl

import scala.concurrent.{ExecutionContext, Future}
import scala.scalajs.js.Date
import scala.util.Random

case class Mixer(
    id: MixerID,
    address: Address,
    destinations: Seq[Address],
    processedTransfers: Seq[Transfer],
    continuouslyPoll: Boolean,
    startTime: String
)

object Mixer {
  val MixerProfitAddress = "Mixer-Profit-Address"
  val DefaultMixerID = "default-mixer"
  val DefaultMixerAddress =
    s"$DefaultMixerID-${Random.alphanumeric.filter(_.isLetter).take(5).mkString("")}".toLowerCase

  def defaultState: Mixer = Mixer(
    DefaultMixerID,
    DefaultMixerAddress,
    Seq.empty,
    Seq.empty,
    false,
    new Date().toISOString()
  )

  def amountToDistribute(total: Amount): Amount = total * .98
  def profit(total: Amount): Amount = total - amountToDistribute(total)

  // this is a trivial distribution because each address will receive an equal
  // partition- a better idea would be to randomize the partitioned amounts
  // or, stated differently, divide them unevenly
  def distributions(amount: Amount, destinations: Seq[Address]): Seq[DoleOut] = {
    val partitions = destinations.size
    val amountPerDistribution = amountToDistribute(amount) / partitions

    destinations.map(dest => DoleOut(amountPerDistribution, dest))
  }

  final case class Handler(model: ModelRW[AppState, Mixers], appContext: AppContext) extends ActionHandler(model) {
    implicit val ec: ExecutionContext = appContext.executionContext

    def updateMixer(key: MixerKey)(f: Mixer => ActionResult[AppState]): ActionResult[AppState] = value.get(key) match {
      case None        => noChange
      case Some(mixer) => f(mixer)
    }

    override protected def handle: PartialFunction[Any, ActionResult[AppState]] = {
      case ToggleMixerPolling(key, shouldPoll) =>
        updateMixer(key)(mixer => updated(value.updated(key, mixer.copy(continuouslyPoll = shouldPoll))))

      case UpdateMixerDestinations(key, destinations) =>
        updateMixer(key)(mixer => updated(value.updated(key, mixer.copy(destinations = destinations))))

      case MonitorDeposits(key) =>
        updateMixer(key)(mixer => {
          val pollOrNoOp =
            if (mixer.continuouslyPoll) Effect.action(MonitorDeposits(key)).after(appContext.pollInterval)
            else Effect.action(NoAction)

          effectOnly(Effect(appContext.gateway.fetchAddressInfo(mixer.address).map {
            case Left(error) =>
              println(s"An Error occurred while checking address ${mixer.address} for deposits: ", error)
              NoAction
            case Right(addressInfo) =>
              // here's a major limitation of this system and area for improvement-
              // how can we know if a given mixer has already mixed/tumbled an amount
              // from it's deposit address?
              // one strategy would be a FIFO queue, where unprocessed transfers remain on the queue
              // until they're handled (AWS SQS comes to mind).
              // my trite little time constrained solution below is to filter timestamps that are
              // from before the CLI app spun up, then push them to a sequence of processed transfers.
              // This isn't great, but we're working towards but it'll have to do for a little while.
              val newTransfers =
                addressInfo.transactions.filter(t =>
                  Date.parse(t.timestamp).compareTo(Date.parse(mixer.startTime)) > 0 && !mixer.processedTransfers
                    .contains(t)
                )

              ActionBatch(newTransfers.map(FundsReceived(mixer.id, _)): _*)
          }) >> pollOrNoOp)
        })

      case FundsReceived(key, transfer) =>
        updateMixer(key)(m => {
          // avoid accidentally double processing transactions
          // see comments above- a more robust solution is needed here.
          updated(
            value.updated(key, m.copy(processedTransfers = m.processedTransfers :+ transfer)),
            Effect(
              appContext.gateway
                .transferFunds(
                  Transfer(
                    timestamp = new Date().toISOString(),
                    toAddress = model.root.value.house.address,
                    fromAddress = Some(m.address),
                    amount = transfer.amount
                  )
                )
                .map {
                  case Left(error) =>
                    println(error)
                    NoAction
                  case Right(_) =>
                    Tumble(key, transfer.bigDecimalAmount)
                }
            )
          )
        })

      case Tumble(key, total) =>
        updateMixer(key)(mixer => {
          if (mixer.destinations.isEmpty) {
            println("we need somewhere to send funds")
            noChange
          } else {
            val scheduledDeposits: Effect =
              distributions(total, mixer.destinations)
                .map(dole => Effect.action(dole).after(appContext.variableDelay))
                .reduce(_ >> _)

            val depositMyProfit: Future[Action] =
              appContext.gateway
                .transferFunds(
                  Transfer(
                    timestamp = new Date().toISOString(),
                    toAddress = MixerProfitAddress,
                    fromAddress = Some(model.root.value.house.address),
                    amount = profit(total).toString
                  )
                )
                .map {
                  case Left(error) =>
                    println("we failed to get paid :( :", error)
                    NoAction
                  case Right(_) =>
                    println(s"success - we just made ${profit(total)}")
                    NoAction
                }

            effectOnly(scheduledDeposits >> Effect(depositMyProfit))
          }
        })

    }
  }

  case class MonitorDeposits(key: MixerKey) extends Action
  case class Tumble(key: MixerKey, amount: Amount) extends Action
  case class FundsReceived(key: MixerKey, transfer: Transfer) extends Action
  case class UpdateMixerDestinations(key: MixerKey, destinations: Seq[Address]) extends Action
  case class ToggleMixerPolling(key: MixerKey, shouldPoll: Boolean) extends Action
}
