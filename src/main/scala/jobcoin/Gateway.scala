package jobcoin

import io.circe.generic.auto._

import scala.concurrent.{ExecutionContext, Future}
import org.scalajs.dom.experimental.{HttpMethod, RequestInit}

import scala.language.{implicitConversions, postfixOps}
import scala.scalajs.concurrent.JSExecutionContext

trait Gateway {
  type ErrorMessage = String
  def checkBalance(address: Address): Future[Either[ErrorMessage, Amount]]
  def fetchAddressInfo(address: Address): Future[Either[ErrorMessage, AddressInfo]]
  def transferFunds(transfer: Transfer): Future[Either[ErrorMessage, Unit]]
}

object Gateway {
  object Service extends Gateway {
    implicit val ex: ExecutionContext = JSExecutionContext.queue
    val APIBase = "http://jobcoin.gemini.com/talon-lettuce/api"

    override def transferFunds(transfer: Transfer): Future[Either[ErrorMessage, Unit]] = {
      NodeFetch
        .request[Unit](
          s"$APIBase/transactions${transfer.queryString}",
          new RequestInit { method = HttpMethod.POST }
        )
        .map {
          case e if e.status == 200 =>
            println(
              s"transfer of ${transfer.amount} from ${transfer.fromAddress.getOrElse("")} to ${transfer.toAddress} successful!"
            )
            Right(())
          case error =>
            println(
              s"transfer of ${transfer.amount} from ${transfer.fromAddress.getOrElse("")} to ${transfer.toAddress} failed!"
            )
            Left(error.response)
        }
    }

    override def fetchAddressInfo(address: Address): Future[Either[ErrorMessage, AddressInfo]] = {
      NodeFetch.request[AddressInfo](s"$APIBase/addresses/$address", new RequestInit { method = HttpMethod.GET }).map {
        case NodeFetch.HttpSuccess(_, info, _)                             => Right(info)
        case NodeFetch.HttpError(status, response)                         => Left(s"error fetching address info")
        case NodeFetch.HttpResponseParsingError(status, message, response) => Left(s"error parsing address info")
      }
    }

    override def checkBalance(address: Address): Future[Either[ErrorMessage, Amount]] =
      fetchAddressInfo(address).map(_.map(_.balance))

    implicit def stringToBigDecimal(s: String): BigDecimal = BigDecimal(s)
  }
}
