package jobcoin

import io.circe.Decoder
import org.scalajs.dom.experimental.{RequestInfo, RequestInit, Response}
import io.circe.parser.decode

import scala.concurrent.{ExecutionContext, Future}
import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

object NodeFetch {

  @js.native
  @JSImport("node-fetch", JSImport.Default)
  object NodeFetchJS extends js.Object {
    def apply(info: RequestInfo): js.Promise[Response] = js.native
    def apply(info: RequestInfo, init: RequestInit): js.Promise[Response] = js.native
  }
  def apply(info: RequestInfo): Future[Response] = NodeFetchJS.apply(info).toFuture
  def apply(info: RequestInfo, init: RequestInit): Future[Response] = NodeFetchJS.apply(info, init).toFuture

  sealed trait HttpResponse[T] {
    val status: Int
    val response: String
  }

  case class HttpError[T](status: Int, response: String) extends HttpResponse[T]
  case class HttpSuccess[T](status: Int, body: T, response: String) extends HttpResponse[T]
  case class HttpResponseParsingError[T](status: Int, message: String, response: String) extends HttpResponse[T]

  def request[T](
      url: String,
      request: RequestInit
  )( //
      implicit
      ec: ExecutionContext,
      decoder: Decoder[T]
  ): Future[HttpResponse[T]] = {
    (for {
      result <- NodeFetch(url, request)
      text <- result.text().toFuture
    } yield {
      result.status match {
        case success if (200 to 299).contains(success) =>
          decode[T](text).fold[HttpResponse[T]](
            e => HttpResponseParsingError(result.status, e.getMessage, text),
            HttpSuccess(result.status, _, text)
          )
        case status => HttpError[T](status, text)
      }
    }).recover { case e: Throwable =>
      println("error :", e)
      HttpError(500, e.getMessage)
    }
  }
}
