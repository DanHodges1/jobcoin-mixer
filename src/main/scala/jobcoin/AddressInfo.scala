package jobcoin

case class AddressInfo(balance: String, transactions: Seq[Transfer]) {
  def balanceAmount: Amount = BigDecimal(balance)
}
