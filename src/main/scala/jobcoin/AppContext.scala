package jobcoin

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration
import scala.scalajs.concurrent.JSExecutionContext

trait AppContext {
  def variableDelay: FiniteDuration
  def pollInterval: FiniteDuration
  def gateway: Gateway
  def executionContext: ExecutionContext
}

object AppContext {
  object NodeApp extends AppContext {
    def variableDelay: FiniteDuration = FiniteDuration(util.Random.shuffle((1 to 30).toList).head, "seconds")
    def pollInterval: FiniteDuration = FiniteDuration(10, "seconds")
    override def gateway: Gateway = Gateway.Service
    override def executionContext: ExecutionContext = JSExecutionContext.queue
  }
}
