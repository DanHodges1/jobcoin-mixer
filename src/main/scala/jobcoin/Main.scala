package jobcoin

import io.scalajs.nodejs
import io.scalajs.nodejs.process.Process
import io.scalajs.nodejs.readline.ReadlineOptions
import jobcoin.Mixer.{DefaultMixerID, MonitorDeposits, ToggleMixerPolling, UpdateMixerDestinations}

object Main {
  object CompletedException extends Exception

  val prompt: String =
    "Please enter a comma-separated list of new, unused Jobcoin addresses where your mixed Jobcoins will be sent."

  val helpText: String =
    """
      |Jobcoin Mixer
      |
      |Takes in at least one return address as parameters (where to send coins after mixing). Returns a deposit address to send coins to.
      |
      |Usage:
      |    run return_addresses...
    """.stripMargin

  def main(args: Array[String]): Unit = {
    nodejs.readline.Readline
      .createInterface(
        ReadlineOptions(input = Process.stdin, output = Process.stdout)
      )
      .question(
        prompt,
        line => {
          if (line == "") {
            println(s"You must specify empty addresses to mix into!\n$helpText")
          } else {
            val addresses: Seq[Address] = line.split(",")

            val defaultMixer: Mixer = RootCircuit
              .zoom(_.mixers(DefaultMixerID))
              .value

            RootCircuit(UpdateMixerDestinations(defaultMixer.id, addresses))
            RootCircuit(ToggleMixerPolling(defaultMixer.id, shouldPoll = true))
            RootCircuit(MonitorDeposits(defaultMixer.id))

            println(
              s"You may now send Jobcoins to address ${defaultMixer.address}.\n\nThey will be mixed and sent to your destination addresses."
            )
          }
        }
      )
  }
}
