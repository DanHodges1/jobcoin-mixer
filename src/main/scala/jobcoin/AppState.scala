package jobcoin

case class AppState(
    mixers: Mixers,
    house: House
)
