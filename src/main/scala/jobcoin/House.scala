package jobcoin

import diode.{Action, ActionHandler, ActionResult, Effect, ModelRW, NoAction}
import jobcoin.Gateway.Service.ErrorMessage

import scala.concurrent.{ExecutionContext, Future}
import scala.scalajs.js.Date

trait House {
  def gateway: Gateway

  def balance: Future[Either[ErrorMessage, Amount]] = gateway.checkBalance(address)
  def address: Address
}

object House {
  case class JobcoinNetworkHouse(gateway: Gateway) extends House {
    val address = "TheGreatHouse"
  }
  final case class Handler(model: ModelRW[AppState, House], appContext: AppContext) extends ActionHandler(model) {

    implicit val gateway: Gateway = appContext.gateway
    implicit val ec: ExecutionContext = appContext.executionContext

    override protected def handle: PartialFunction[Any, ActionResult[AppState]] = { case DoleOut(amount, destination) =>
      effectOnly(
        Effect(
          value.balance
            .map[Future[Action]] {
              case Left(errorMsg) =>
                println(s"insufficient funds in house :", errorMsg)
                Future.successful(NoAction)
              case Right(balance) =>
                if (balance < amount) {
                  println(s"house balance of $balance is insufficient to transfer $amount to $destination")
                  Future.successful(NoAction)
                } else {
                  appContext.gateway
                    .transferFunds(
                      Transfer(
                        new Date().toISOString(),
                        toAddress = destination,
                        fromAddress = Some(value.address),
                        amount = amount.toString()
                      )
                    )
                    .map {
                      case Left(error) =>
                        println(s"transfer of $amount to $destination failed :", error)
                        NoAction
                      case Right(_) =>
                        println(s"$amount successfully transferred to $destination")
                        NoAction
                    }
                }
            }
            .flatten
        )
      )
    }
  }

  case class DoleOut(amount: Amount, address: Address) extends Action
}
