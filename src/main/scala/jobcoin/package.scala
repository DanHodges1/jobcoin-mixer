package object jobcoin {
  type Address = String
  type Amount = BigDecimal
  type MixerID = String
  type MixerKey = String
  type Mixers = Map[MixerKey, Mixer]
}
