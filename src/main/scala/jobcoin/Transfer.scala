package jobcoin

case class Transfer(timestamp: String, toAddress: Address, fromAddress: Option[Address], amount: String) { self =>
  def bigDecimalAmount: BigDecimal = BigDecimal(self.amount)
  def queryString =
    s"?${fromAddress.fold("")(from => s"fromAddress=$from&")}toAddress=$toAddress&amount=$amount"
}
