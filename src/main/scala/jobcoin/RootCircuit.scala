package jobcoin

import diode._
import jobcoin.House.JobcoinNetworkHouse
import jobcoin.Mixer.DefaultMixerID

trait RootCircuitOps extends Circuit[AppState] {

  def initialModel: AppState = AppState(
    mixers = Map(DefaultMixerID -> Mixer.defaultState),
    house = JobcoinNetworkHouse(appContext.gateway)
  )

  def appContext: AppContext

  override protected def actionHandler: RootCircuit.HandlerFunction =
    foldHandlers(
      Mixer.Handler(zoomTo(_.mixers), appContext),
      House.Handler(zoomTo(_.house), appContext)
    )
}

object RootCircuit extends RootCircuitOps {
  override def appContext: AppContext = AppContext.NodeApp
}
