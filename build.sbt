import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport.scalaJSStage

val circeVersion = "0.14.1"

lazy val root = project
  .in(file("."))
  .enablePlugins(ScalaJSPlugin, ScalaJSBundlerPlugin)
  .settings(
    name := "jobcoin-mixer",
    version := "0.1",
    scalaVersion := "2.13.6",
    scalaJSLinkerConfig ~= { _.withModuleKind(ModuleKind.CommonJSModule) },
    Global / onChangedBuildSource := ReloadOnSourceChanges,
    scalaJSStage in Global := FastOptStage,
    scalaJSUseMainModuleInitializer := true,
    Compile / npmDependencies += "node-fetch" -> "2.6.1",
    webpackConfigFile :=
      Some(baseDirectory.value / "src" / "main" / "resources" / "webpack.js"),
    libraryDependencies ++= Seq(
      "io.suzaku" %%% "diode" % "1.1.13",
      "org.scala-js" %%% "scalajs-dom" % "1.1.0",
      "net.exoego" %%% "scala-js-nodejs-v14" % "0.13.0",
      "org.scalatest" %%% "scalatest" % "3.2.8" % Test
    ) ++ Seq(
      "io.circe" %%% "circe-core",
      "io.circe" %%% "circe-generic",
      "io.circe" %%% "circe-parser"
    ).map(_ % circeVersion)
  )
